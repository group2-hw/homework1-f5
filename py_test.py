## import urllib
import sys
import requests
import pytest

link = "https://the-internet.herokuapp.com/context_menu"  

def request():
## this is HTTP GET request
    response = requests.get(link)

    ##checks if fetching the url was successfull = 200
    if response.status_code == 200:
        ##print(response.text)
        res_txt = response.text

    return res_txt

##this is the first Test - Pass if "Right click.." string is found in url
def test_find_string():
    r = request()
    find_string = "Right-click in the box below to see one called 'the-internet'"  
    assert find_string in r, f"'{find_string}' not found on the page."
    
##this is the second Test - Fails is Alibaba is not found on page
def test_Alibaba():
    r2 = request()
    find_string2 = "Alibaba"
    assert find_string2 in r2, f" Failed,'{find_string2}' not found on the page."


##if __name__ == "__main__":
 # test_find_string()
  ##test_Alibaba()

